use std::time;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::path::PathBuf;
use std::io;
use std::io::{Read, Write};
use std::os::unix::net::{UnixStream, UnixListener};
use std::os::unix::io::AsRawFd;
use mio::{Poll, PollOpt, Token, Evented, Events, Ready};
use mio::unix::EventedFd;
use usbapi::{UsbEnumerate, UsbCore};
use signal_hook;
struct IpcServer {
    handle: UnixListener,
    socket_file: PathBuf,
}


impl IpcServer {
    fn new(socket_file: PathBuf) -> Self {
        let handle = UnixListener::bind(&socket_file).expect("Could not create socket");
        handle.set_nonblocking(true).expect("Could not set it non blocking");
        Self {
            socket_file,
            handle
        }
    }

    fn accept(&self) -> Option<UnixStream> {
        match self.handle.accept() {
            Ok((stream, addr)) => {
                println!("caller {:?}", addr);
                Some(stream)
            }
            Err(_) => { None }
        }
    }
}

// Remove socket_file when go out of scope
impl Drop for IpcServer {
    fn drop(&mut self) {
        // Ignore possible error.
        std::fs::remove_file(&self.socket_file).unwrap_or_else(|_|{ println!("{:?} already removed?", self.socket_file);});
    }
}

impl Evented for IpcServer {
    fn register(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.handle.as_raw_fd()).register(poll, token, interest, opts)
    }

    fn reregister(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.handle.as_raw_fd()).reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &Poll) -> io::Result<()> {
        EventedFd(&self.handle.as_raw_fd()).deregister(poll)
    }
}

#[derive(Debug)]
struct IpcClient(UnixStream);
impl Evented for IpcClient {
    fn register(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.0.as_raw_fd()).register(poll, token, interest, opts)
    }

    fn reregister(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.0.as_raw_fd()).reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &Poll) -> io::Result<()> {
        EventedFd(&self.0.as_raw_fd()).deregister(poll)
    }
}

fn setup_signal_handlers() -> Result<Arc<AtomicBool>, std::io::Error> {
    let sigterm = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGQUIT, sigterm.clone())?;
    signal_hook::flag::register(signal_hook::SIGTERM, sigterm.clone())?;
    signal_hook::flag::register(signal_hook::SIGINT, sigterm.clone())?;
    Ok(sigterm)
}

fn main() {
    let sigterm = setup_signal_handlers().expect("Could not setup signal handlers");
    let mut usb = UsbEnumerate::new();
    usb.enumerate().expect("Could not enumerate USB devices.");
    println!("{}", toml::to_string(&usb.devices()).unwrap_or("".to_string()));

    let poll = Poll::new().expect("Poll could not be initialized");
    let listener = IpcServer::new(PathBuf::from("/run/kindofawkward.sock"));
    listener.register(&poll, Token(0), Ready::readable(), PollOpt::level()).expect("Could not register poll");
    let mut events = Events::with_capacity(8);
    let mut streams = std::collections::HashMap::new();
    loop {
        if let Err(err) = poll.poll(&mut events, Some(time::Duration::from_millis(250))) {
            eprintln!("{}", err);
            break;
        }
        for ev in &events {
            match ev.token() {
                Token(0) => {
                    if ev.readiness() != Ready::readable() {
                        panic!("readiness false");
                    }

                    if let Some(stream) = listener.accept() {
                        let stream = IpcClient(stream);
                        stream.register(&poll, Token(1 + streams.len()), Ready::readable(), PollOpt::level()).expect("Could not register poll");
                        if stream.0.set_nonblocking(true).is_ok() {
                            streams.insert(Token(1 + streams.len()), stream);
                            println!("{:?}", &streams);
                        } else {
                            eprintln!("Could not set to non blocking, connection dropped.");
                        }
                    }
                }
                token => {
                    if ev.readiness() != Ready::readable() {
                        println!("Remote connection closed");
                        streams.remove(&token);
                    } else {
                        let stream = streams.get_mut(&token).unwrap();
                        let mut rx: [u8; 32] = [0; 32];
                        if stream.0.read(&mut rx).is_ok() {
                            let debug = format!("TODO send to USB:\n'{}'", String::from_utf8_lossy(&rx));
                            stream.0.write(debug.as_bytes()).unwrap_or(0);
                        }
                    }
                }
            }
        }
        if sigterm.load(Ordering::Relaxed) {
            break;
        }
    }
  
    println!("Terminated gracefully");
}
